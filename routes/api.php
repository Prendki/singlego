<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::prefix('user')->group(function() {
        //user
        Route::get('/', 'Api\UserController@index');

        //pary
        Route::get('/pairs', 'Api\PairsController@index');

        Route::get('/favourites', 'Api\PairsController@getFavourites');

        Route::get('/pairs/{id}', 'Api\PairsController@show');

        Route::post('/pairs','Api\PairsController@create');
        Route::delete('/pairs/{id}','Api\PairsController@delete');

        //znajdowanie
        Route::get('/find', 'Api\FinderController@show');

        //wiadomości

        //Pobierz wszystkie
        Route::get('/messages', 'Api\MessageController@index');

        //Pobierz wiadomości tylko z konkretnym ziomkiem
        Route::get('/messages/recipient/{id}', 'Api\MessageController@show');

        //Wyslij wiadomośc do konkretnego usera
        Route::post('/messages/recipient/{id}', 'Api\MessageController@store');

//        Usuń wiadomość
        Route::delete('/messages/{id}', 'Api\MessageController@destroy');

    });
});

