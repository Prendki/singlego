<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!z
|
*/

Auth::routes();

Route::middleware(['preauth'])->group(function ()  {
    Route::get('/', 'HomeController@index')->name('home');
});

Route::middleware(['auth'])->group(function () {
    //profil
    Route::prefix('profil')->group(function() {
        Route::get('/', 'Profile\ProfileController@index');
        Route::get('/{id}', 'Profile\ProfileController@show');
    });
    //ustawienia
    Route::prefix('ustawienia')->group(function() {
        Route::get('/', 'Profile\SettingsController@index');
        Route::post('/', 'Profile\SettingsController@update');
    });
    Route::prefix('szukaj')->group(function() {
        Route::get('/', 'Profile\FinderController@index');
    });

    Route::get('ulubione',"Profile\FavouriteController@index");
    Route::get('wiadomosci/{id}',"Profile\MessagesController@show");
    Route::get('pary',"Profile\PairsController@index");
});


