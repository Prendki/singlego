<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    protected $fillable = [
        'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function target_user() {
        return $this->belongsTo('App\User');
    }
}
