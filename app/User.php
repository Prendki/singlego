<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','last_name', 'email', 'password','gender','api_token','description','interested_in',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function actions() {
        return $this->hasMany('App\Reaction','user_id');
    }

    public function reactions() {
        return $this->hasMany('App\Reaction','target_user_id');
    }

    public function messages_to() {
        return $this->hasMany('App\Message','user_id');
    }

    public function messages_from() {
        return $this->hasMany('App\Message','target_user_id');
    }

    public function photos() {
        return $this->hasMany('App\Photo','user_id');
    }
}
