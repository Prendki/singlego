<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FinderController extends Controller
{
    public function index() {
        return view('profile.find');
    }
}
