<?php

namespace App\Http\Controllers\Profile;

use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        return view('profile.home',['user'=>$user]);
    }
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return view('profile.home',['user'=>$user]);
    }
}
