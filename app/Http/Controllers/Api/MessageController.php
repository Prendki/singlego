<?php

namespace App\Http\Controllers\Api;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request)
    {
        $user = $request->user();

        return DB::table('messages')
            ->select('messages.*')
            ->join("users as u1", function ($join) {
                $join->on('messages.user_id', '=', 'u1.id');
            })
            ->join("users as u2", function ($join) {
                $join->on('messages.target_user_id', '=', 'u2.id');
            })
            ->where('messages.target_user_id', '=', $user->id)
            ->orWhere('messages.user_id', '=', $user->id)
            ->orderBy('messages.target_user_id')
            ->get();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $user = $request->user();
        $target_user = User::find($id);
        $message = new Message();
        $message->text = Input::get('text');
        $message->target_user()->associate($target_user);
        return $user->messages_to()->save($message);
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Message $message
     * @return \Illuminate\Support\Collection
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();
        $target_user = User::find($id);

        return DB::table('messages')
            ->select('messages.*')
            ->join("users as u1", function ($join) {
                $join->on('messages.user_id', '=', 'u1.id');
            })
            ->join("users as u2", function ($join) {
                $join->on('messages.target_user_id', '=', 'u2.id');
            })
            ->where([
                ['messages.target_user_id', '=', $user->id],
                ['messages.user_id', '=', $target_user->id]
            ])
            ->orWhere([
                ['messages.user_id', '=', $user->id],
                ['messages.target_user_id', '=', $target_user->id]
            ])
            ->orderBy('messages.target_user_id')
            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request $request
     */
    public function destroy(Request $request,$id)
    {
        $user = $request->user();
        $message = Message::find($id);
        return $user->messages_to()->delete($message);
    }
}
