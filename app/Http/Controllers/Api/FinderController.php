<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class FinderController extends Controller
{

    public function index(Request $request) {
        $user = $request->user();

        return DB::table('users as u')
            ->select('u.*')
            ->leftJoin("reactions as r",function($join) use($user) {
                $join->on('u.id','=','r.target_user_id');
                $join->on('r.user_id','=',DB::raw($user->id));
            })
            ->where([
                ['u.id','!=',$user->id],
            ])
            ->whereNull('r.id')
            ->get();
    }

    public function show(Request $request) {
        $user = $request->user();

        $targets = DB::table('users as u')
            ->select('u.*')
            ->leftJoin("reactions as r",function($join) use($user) {
                $join->on('u.id','=','r.target_user_id');
                $join->on('r.user_id','=',DB::raw($user->id));
            })
            ->where([
                ['u.id','!=',$user->id],
                ['u.gender','=',$user->interested_in]
            ])
            ->whereNull('r.id')
            ->get();

        if(sizeof($targets)==0) {
            return Response::json(false);
        }

        $target_id = rand(0,sizeof($targets)-1);
        $target = $targets[$target_id];
        $target = User::with("photos")->find($target->id);
        return Response::json($target);
    }
}
