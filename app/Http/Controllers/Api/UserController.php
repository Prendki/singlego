<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        return $user->with('photos')->where('id','=',$user->id)->get();
    }
}
