<?php

namespace App\Http\Controllers\Api;

use App\Reaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PairsController extends Controller
{
    public $successStatus = 200;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getFavourites(Request $request)
    {
        $user_id = $request->user()->id;

        return DB::table('reactions as p1')
            ->select('u1.*', 'photos.*')
            ->join('users as u1', function ($join) {
                $join->on('u1.id', '=', 'p1.target_user_id');
            })
            ->join('photos', function ($join) {
                $join->on('u1.id', '=', 'photos.user_id');
            })
            ->where([
                ['p1.user_id', '=', $user_id],
                ['p1.type', '=', '1'],
            ])
            ->get();
    }

    public function index(Request $request)
    {
        $user_id = $request->user()->id;

        return DB::table('reactions as p1')
            ->select('p3.*')
            ->join("reactions as p2", function ($join) {
                $join->on('p2.user_id', '=', 'p1.target_user_id');
                $join->on('p2.target_user_id', '=', 'p1.user_id');
            })->join("users as p3", function ($join) {
                $join->on('p2.user_id', '=', 'p3.id');
            })
            ->where([
                ['p1.user_id', '=', $user_id],
                ['p1.type', '=', '1'],
                ['p2.type', '=', '1']
            ])
            ->get();
    }

    public function show(Request $request, $id)
    {
        $user_id = $request->user()->id;
        return Reaction::where([
            ['user_id', '=', $user_id],
            ['id', '=', $id]
        ])->get();
    }

    public function create(Request $request)
    {
        $user = new User();
        $auth_user = $request->user();
        $target_user = $user->find(Input::get('target_user_id'));
        $reaction = new Reaction;
        $reaction->type = 1;
        $reaction->target_user()->associate($target_user);
        return $auth_user->actions()->save($reaction);
    }

    public function delete(Request $request, $id)
    {
        $user = new User();
        $action = Reaction::find($id);
        $auth_user = $request->user();
        return $auth_user->actions()->delete($action);
    }
}
