@extends('layouts.app')


@section('content')
<header>
    <div class="container py-5 h-100">
        <div class="row">
            <div class="col-lg-3 mt-5">
                <div class="menu">
                    <ul>
                        <li>
                            <a href="/profil" class="link"><i class="fas fa-book mr-2"></i>Profil</a>
                        </li>
                        <li>
                            <a href="/szukaj" class="link"><i class="far fa-grin-hearts mr-2"></i> Losuj i znajdź</a>
                        </li>
                        <li>
                            <a href="/ulubione" class="link"><i class="fas fa-heart mr-2"></i>Ulubione</a>
                        </li>
                        <li>
                            <a href="/pary" class="link"><i class="	fas fa-users mr-2"></i>Pary</a>
                        </li>
                        <li>
                            <a href="/ustawienia" class="link"><i class="fas fa-book mr-2"></i> Ustawienia</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" class="link" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fas fa-times mr-2"></i> Wyloguj</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 d-flex justify-content-center mt-5">
                @yield('sub_content')
            </div>
        </div>
    </div>
</header>
@endsection
