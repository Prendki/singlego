@extends('layouts.app')

@section('content')
   <header>
       <div class="container py-5 h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-lg-6">
                    <h1>Już teraz <span class="bold">Poznaj</span> swoją <span class="bold">drugą</span> połówkę</h1>
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="/register" class="btn btn-dark">Zarejestruj się</a>
                        </div>
                        <div class="col-lg-6">
                            <a href="/login" class="btn btn-dark">Zaloguj się</a>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </header>
@endsection

