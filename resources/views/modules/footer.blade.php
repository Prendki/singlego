<footer id="footer">
    <a href="#footer" class="anchor-top">
        <i class="fas fa-arrow-up"></i>
    </a>
    <div class="container">
        <ul>
            <li>
                <a href="http://lovetime.pl/smieszne-teksty-na-podryw.php">Jak znaleźć miłość w internecie?</a>
            </li>
            <li>
                <a href="#">Coś tam</a>
            </li>
            <li>
                <a target="_blank" href="https://www.youtube.com/watch?v=XXaOU5YVDI0">Co to jest miłość?</a>
            </li>
        </ul>
    </div>
    <div class="container text-center">
        2019 &copy; M. Przybył, M. Prendki, M. Skrzypek, M. Voss
    </div>
</footer>
