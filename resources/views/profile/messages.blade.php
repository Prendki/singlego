@extends('layouts.profile')

@section('sub_content')
    <section class="w-100 profile-view p-5">
        <message-component :user="{{ json_encode($user) }}"></message-component>
    </section>
@endsection