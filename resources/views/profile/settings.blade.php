@extends('layouts.profile')

@section('sub_content')
    <section class="w-100 profile-view p-5">
        <div class="profile-view">
            <h1>Ustawienia</h1>

            <form method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="f-w-light f-s-upper">Imię</label><br />
                            <input type="text" value="{{$user->name}}" name="name"/>
                        </div>
                        <div class="form-group">
                            <label class="f-w-light f-s-upper">nazwisko</label><br />
                            <input type="text" value="{{$user->last_name}}" name="last_name"/>
                        </div>
                        <div class="form-group">
                            <label class="f-w-light f-s-upper">opis</label><br />
                            <textarea type="text" name="description">{{$user->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="f-w-light f-s-upper">Płeć</label><br />
                            <select name="gender">
                                <option value="0" {{$user->gender == 0 ? 'selected': null}}>
                                    Kobieta
                                </option>
                                <option value="1" {{$user->gender == 1 ? 'selected': null}}>
                                    Mężczyzna
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="f-w-light f-s-upper">Podobają ci się</label><br />
                            <select name="interested_in">
                                <option value="0" {{$user->interested_in == 0 ? 'selected': null}}>
                                    Kobiety
                                </option>
                                <option value="1" {{$user->interested_in == 1 ? 'selected': null}}>
                                    Mężczyźni
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <p class="f-w-light f-s-upper">Twoje zdjęcia</p>
                        <app-settings-photos></app-settings-photos>
                        <div class="mt-3">
                            <p class="f-w-light f-s-upper">Dodaj nowe zdjęcie</p>
                            <input multiple="multiple" name="photos[]" type="file">
                        </div>
                    </div>
                </div>
                <button type="submit">
                    Zapisz
                </button>
            </form>
        </div>
    </section>
@endsection