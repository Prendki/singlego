import Swiper from 'swiper';

export default class {
    constructor() {
        // this.swiperProfile();
    }

    swiperProfile() {
        $(function () {
            var swiper = new Swiper('.swiper-container', {
                effect: 'coverflow',
                grabCursor: true,
                centeredSlides: true,
                slidesPerView: '5',
                initialSlide:2,
                coverflowEffect: {
                    rotate: 20,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true,
                },
                pagination: {
                    el: '.swiper-pagination',
                },
            });
        })
    }
};
