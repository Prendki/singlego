<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'test1',
            'email' => 'test1@gmail.com',
            'gender' => 1,
            'interested_in'=>0,
            'api_token' => Str::random(60),
            'password' => bcrypt('test1'),
        ]);
        factory(App\User::class)->create([
            'name' => 'test2',
            'email' => 'test2@gmail.com',
            'gender' => 0,
            'interested_in'=>0,
            'api_token' => Str::random(60),
            'password' => bcrypt('test2'),
        ]);
        factory(App\User::class)->create([
            'name' => 'test3',
            'email' => 'test3@gmail.com',
            'gender' => 1,
            'interested_in'=>0,
            'api_token' => Str::random(60),
            'password' => bcrypt('test3'),
        ]);


        $user = new \App\User();
        $reaction = new \App\Reaction();
        $reaction->type = 1;
        $user1 = $user->find(1);
        $user2 = $user->find(2);


        $reaction->target_user()->associate($user2);
        $user1->actions()->save($reaction);

        $mens = factory(App\User::class, 10)->create(['gender'=>'1','interested_in'=>'0'])->each(function($u){
            $user_id = $u->id;
            $photo = new \App\Photo();
            $photo->filename = "photos/man.jpg";
            $photo->user_id = $user_id;
            $photo->user()->associate($u);
            $photo->save();
            for($i = 0;$i < 8;$i++){
                do {
                    $target_user_id = rand(1,53);
                } while(in_array($target_user_id, array($user_id)));
                $messages = factory(\App\Message::class)->create(
                    ['user_id'=>$user_id,'target_user_id'=>$target_user_id]
                );
            }
        });
        $womens = factory(App\User::class, 10)->create(['gender'=>'0','interested_in'=>'1'])->each(function($u){
            $user_id = $u->id;
            $photo = new \App\Photo();
            $photo->filename = "photos/woman.jpg";
            $photo->user_id = $user_id;
            $photo->user()->associate($u);
            $photo->save();
            for($i = 0;$i < 8;$i++){
                do {
                    $target_user_id = rand(1,53);
                } while(in_array($target_user_id, array($user_id)));
                $messages = factory(\App\Message::class)->create(
                    ['user_id'=>$user_id,'target_user_id'=>$target_user_id]
                );
            }
        });

//        $messages = factory(\App\Message::class,10)->create();



    }
}
